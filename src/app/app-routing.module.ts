import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Lazy loads
const routes:Routes=[
  {
    path:'home',
    loadChildren:()=>import('./components/home/home.module').then(m=>m.HomeModule)
  },
  {
    path:'about',
    loadChildren:()=>import('./components/about/about.module').then(m=>m.AboutModule)
  },
  {
    path:'heroes',
    loadChildren:()=>import('./components/heroes/heroes.module').then(m=>m.HeroesModule)
  },
  // {
  //   path:'heroe/:id',
  //   loadChildren:()=>import('./components/heroe/heroe.module').then(m=>m.HeroeModule)
  // },
  {
  path:'',redirectTo:'home',pathMatch:'full'
  },
  {
    path:'**',redirectTo:'home',pathMatch:'full'
  }
]

@NgModule({
  imports: [
   RouterModule.forRoot(routes)
  ],
  exports:[
    RouterModule
  ]
})
export class AppRoutingModule { }
