import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
//ruta principales
import { AppRoutingModule } from './app-routing.module';

// servicios
import { HeroesService } from './servicios/heroes.service';

// componentes
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';




@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [HeroesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
