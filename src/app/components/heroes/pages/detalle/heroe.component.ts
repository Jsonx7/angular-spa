import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HeroesService } from '../../../../servicios/heroes.service';

@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html'
})
export class HeroeComponent {
  
  heroe:any={}
  casaDC=false;
  constructor(private activatedRoute:ActivatedRoute,private _heroesService:HeroesService) { 
   this.activatedRoute.params.subscribe(params=>{
     this.heroe=_heroesService.getHeroe(params['id']);
     if (this.heroe.casa=='DC') {
       this.casaDC=true;
     }
   });
  }
  

}
