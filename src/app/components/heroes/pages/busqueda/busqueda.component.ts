import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HeroesService } from '../../../../servicios/heroes.service';

@Component({
  selector: 'app-busqueda',
  templateUrl: './busqueda.component.html'
})
export class BusquedaComponent implements OnInit {
    heroes:any[]=[]
    valorbusqueda!:string;
  constructor(private _heroesService:HeroesService,private _activatedRoute:ActivatedRoute) { }

  ngOnInit(): void {
        this._activatedRoute.params.subscribe(params=>{
          this.valorbusqueda=params['valor'];
              this.heroes=this._heroesService.buscarHeroes(params['valor']);
        });
  }
  


}
