import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HeroesComponent } from './pages/listado/heroes.component';
import { HeroeComponent } from './pages/detalle/heroe.component';
import { BusquedaComponent } from './pages/busqueda/busqueda.component';

const routes: Routes = [
  {
    path:'',
    children:[
      {
        path:'listado', component:HeroesComponent
      },
      {
        path:'heroe/:id', component:HeroeComponent
      },
      {
        path:'busqueda/:valor', component:BusquedaComponent
      }   
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HeroesRoutingModule { }
